import math

def rectArea(width, length):
  area = width * length;
  return area;

def circleArea(radius):
  area = math.pi * (radius ** 2);
  return area;

def triangleAreaSides(side1, side2, side3):
  param = (side1 + side2 + side3) / 2;
  area = math.sqrt(param * (param - side1) * (param - side2) * (param - side3));
  return area;

def triangleAreaAngle(side1, side2, angle):
  area = side1 * side2 * (math.sin(math.radians(angle)) / 2)
  return area;
