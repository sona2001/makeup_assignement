import utils
import math

while(True):

  shape = int(input('Input the shape: \n 1 for Rectangle, \n 2 for Circle \n 3 for Triangle using sides only \n 4 for Triangle using sides and and angle \n 5 for Quit \n'));

  if(shape == 1):

    side1 = int(input('Insert side 1 = '));
    side2 = int(input('Insert side 2 = '));

    print(utils.rectArea(side1, side2));


  elif(shape == 2):

    radius = int(input('Insert radius of circle = '));

    print(round(utils.circleArea(radius), 2));


  elif(shape == 3):

    side1 = int(input('Insert side 1 = '));
    side2 = int(input('Insert side 2 = '));
    side3 = int(input('Insert side 3 = '));

    print(utils.triangleAreaSides(side1, side2, side3));

    
  elif(shape == 4):

    side1 = int(input('Insert side 1 = '));
    side2 = int(input('Insert side 2 = '));
    angle = int(input('Insert an angle = '));

    print(utils.triangleAreaAngle(side1, side2, angle));

  elif(shape == 5):
    print('Bye!');
    break;